# cas-server-webapp-4.0.7

#### 介绍


> CAS是Central Authentication Service的缩写，中央认证服务，一种独立开放指令协议。CAS 是 耶鲁大学（Yale University）
> 发起的一个开源项目，旨在为 Web 应用系统提供一种可靠的单点登录方法，CAS 在 2004 年 12 月正式成为 JA-SIG 的一个项目;

### 开源的企业级单点登录解决方案
> 企业使用时还需要对源码进行定制改造，本教程讲解从源码到改造上线的大体过程，及单点的使用。更多需求可留言交流学习。
#### 软件架构
SSH+mysql5.6+jdk1.7
#### [源码改造](https://www.yuque.com/cgqpgn/apy7im)
1. [第一篇、项目初始化](https://www.yuque.com/cgqpgn/apy7im/sgm7cv)
2. [第二篇、数据库验证登陆](https://www.yuque.com/cgqpgn/apy7im/rnnntk)
3. [第三篇、自定义多页面](https://www.yuque.com/cgqpgn/apy7im/wx0nm4)
4. [第四篇、登录验证码](https://www.yuque.com/cgqpgn/apy7im/qpt9nc)
5. [第五篇、客户端集成实现](https://www.yuque.com/cgqpgn/apy7im/opf5gi)
6. [第六篇、单点返回更多用户信息](https://www.yuque.com/cgqpgn/apy7im/rrp04f)
7. [第七篇、单点Oauth实现](https://www.yuque.com/cgqpgn/apy7im/ef711c)



#### 安装教程

1.  下载代码导入IDEA
2.  代码中有脚本，mysql5.6中执行脚本
3.  修改cas.properties配置文件中数据库配置
4.  项目设置contextpath=/并启动

#### 使用说明

1.  访问：localhost:8001/login
2.  账号密码：admin 8888
3.  访问多页面：localhost:8001/login?layout=custom1 或 localhost:8001/login?layout=custom2
4.  其他的请参考源码改造教程

