package com.mac.sso.flow;

import org.springframework.webflow.core.collection.ParameterMap;
import org.springframework.webflow.execution.RequestContext;

/**
 * 根据请求参数自动更换登录页面
 * layout=?
 */
public class SwitchLoginViewAction {

    String defultview="casLoginView";

    public final String checkLoginView(RequestContext context) {
        ParameterMap requestParameters = context.getRequestParameters();
        String layout = requestParameters.get("layout");
        if(layout==null||"".equals(layout)){
            return defultview;
        }
        //规则暂定这样
        return layout+"CasLoginView";
    }
}
