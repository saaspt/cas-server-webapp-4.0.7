package com.mac.sso.dao;

import com.mac.sso.bean.UserInfo;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import javax.annotation.Resource;

/**
 * @see UserInfo
 * @author byy
 */
@Repository 
public class UserInfoDAO {
	private static final Logger log = LoggerFactory.getLogger(UserInfoDAO.class);

	public UserInfo findByUsername(String username){
		Query queryObject = getSession().createQuery("from UserInfo as u where u.username=:username");
		Object u= queryObject.setString("username", username).uniqueResult();

		return (UserInfo) u;
	}

	@Resource
	private SessionFactory sessionFactory;
	private Session session;


	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session getSession() {
		this.session = sessionFactory.getCurrentSession();
		return session;
	}
	public void setSession(Session session) {
		this.session = session;
	}

}