package com.mac.sso.authentication;

import cn.hutool.crypto.SecureUtil;
import com.mac.sso.bean.UserInfo;
import com.mac.sso.bean.UsernamePasswordCredentialWithAuthCode;
import com.mac.sso.service.UserInfoService;
import org.apache.commons.lang.StringUtils;
import org.jasig.cas.authentication.HandlerResult;
import org.jasig.cas.authentication.UsernamePasswordCredential;
import org.jasig.cas.authentication.handler.support.AbstractUsernamePasswordAuthenticationHandler;
import org.jasig.cas.authentication.principal.SimplePrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.security.auth.login.FailedLoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.GeneralSecurityException;

/**
 * @author： byy
 * @date : 2017年11月23日 下午4:59:32
 * @Description：自定义数据库方式认证类
 */
public class SaaSAuthenticationHandler extends AbstractUsernamePasswordAuthenticationHandler {

    @Autowired
    private UserInfoService userInfoService;



    protected HandlerResult authenticateUsernamePasswordInternal(UsernamePasswordCredential transformedCredential) throws GeneralSecurityException {
        //UsernamePasswordCredential参数包含了前台页面输入的用户信息
        String username = transformedCredential.getUsername().
                replaceAll(" ", "").toLowerCase();//需要剔除空格,大写转小写
        String password = transformedCredential.getPassword();
        UserInfo userInfo = userInfoService.findByUsername(username);
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.
                getRequestAttributes()).getRequest();

        request.setAttribute("username", username);//防止登录报错，再输入账户
        //1.验证码是否正确
        if(!validatorCode(transformedCredential)){
            request.setAttribute("msg", "验证码错误");
            request.setAttribute("password", password);
            throw new FailedLoginException();
        }

        //2.用户名是否存在验证
        if (userInfo == null) {//用户名错误用此异常
            request.setAttribute("msg", "用户名或密码错误");
            throw new FailedLoginException();
        }

        String md5password = SecureUtil.md5(password);
        //3.验证密码是否正确
        if (!md5password.equals(userInfo.getPassword())) {
            request.setAttribute("msg", "用户名或密码错误");
            throw new FailedLoginException();
        }

        //4.返回登陆成功状态，CAS程序继续运行
        return createHandlerResult(transformedCredential, new SimplePrincipal(username), null);
    }

    /**
     * 验证码验证工具
     * @param transformedCredential
     * @return
     */
    private boolean validatorCode(UsernamePasswordCredential transformedCredential) {
        UsernamePasswordCredentialWithAuthCode upc = (UsernamePasswordCredentialWithAuthCode) transformedCredential;
        String submitAuthcode = upc.getAuthcode();

        HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession();
        //session中获取谷歌验证码
        String authcode = (String) session.getAttribute(com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY);
        session.removeAttribute(com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY);//验证一次即作废
        if (StringUtils.isEmpty(submitAuthcode) || StringUtils.isEmpty(authcode)) {
            System.out.println("验证码为空OR验证码未生成");
        }
        if (submitAuthcode.equals(authcode)) {
            System.out.println("验证码正确");
            return true;
        }
        return false;
    }

}  